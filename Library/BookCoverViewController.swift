//
//  BookCoverViewController.swift
//  Library
//
//  Created by Santiago Dameno on 16/11/18.
//  Copyright © 2018 Santiago Dameno. All rights reserved.
//

import UIKit

class BookCoverViewController: UIViewController {

    @IBOutlet var bookCoverView: UIImageView!
    
    var book: [String: String]!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        if let fileName = book["Cover"] {
            bookCoverView.image = UIImage(named: fileName)
            bookCoverView.contentMode = .scaleAspectFit
        }

        
        // Do any additional setup after loading the view.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
